package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Grade extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade);
        CardView ninth = (CardView)findViewById(R.id.ninth);
        ninth.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                OpenSubject(v);
            }
        });
        CardView tenth = (CardView)findViewById(R.id.tenth);
        tenth.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                OpenSubject(v);
            }
        });
        CardView eleventh = (CardView)findViewById(R.id.eleventh);
        eleventh.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                OpenSubject(v);
            }
        });
        CardView twelveth = (CardView)findViewById(R.id.twelveth);
        twelveth.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                OpenSubject(v);
            }
        });
    }

    public void OpenSubject(View view){
        Intent intent = new Intent(this, Subject.class);
        startActivity(intent);
    }
}
